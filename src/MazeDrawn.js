import React from 'react'
import stickFigure from './stick-figure3.png'
// const { getFurthestMazeEnd, getBottomRightEnd, getFurthestEdgeMazeEnd } = require('./getMazeEnd') 

const MazeDrawn = (props) => {
    const maze = props.maze || []
    const endings = props.mazeEndings
    const difficulty = props.difficulty
    const currentLocation = props.currentLocation
    const visitedPaths = props.visitedPaths
    // const mazeEndOne = getFurthestEdgeMazeEnd(maze)
    // const mazeEndTwo = getBottomRightEnd(maze)
    // const mazeEndThree = getFurthestMazeEnd(maze)

    const getStyle = (x, y, walls) => {
        let backgroundImage = false
        let backgroundColor = 'inherit'
        if(visitedPaths[difficulty][`x${x}y${y}`]) {
            backgroundColor = '#fffcbf'
        }
        if(x === 0 && y === 0){
            backgroundColor = 'green'
        }
        if(x === endings[difficulty].x && y === endings[difficulty].y){
            backgroundColor = 'red'
        }
        if((x === currentLocation[difficulty][0]) && (y === currentLocation[difficulty][1])){
            backgroundImage = `url(${stickFigure})`
        }
        // if(x === mazeEndOne.x && y === mazeEndOne.y){
        //     backgroundColor = 'yellow'
        // }
        // if(x === mazeEndTwo.x && y === mazeEndTwo.y){
        //     backgroundColor = 'cyan'
        // }
        let width
        let height
        if (walls.includes('left') && walls.includes('right')) {
            width = 20
        } else
         if (walls.includes('left') || walls.includes('right')) {
            width = 22
        } else {
            width = 24
        }
        if (walls.includes('up') && walls.includes('down')) {
            height = 20
        } else 
        if (walls.includes('up') || walls.includes('down')) {
            height = 22
        } else {
            height = 24
        }
        return {
            width: width,
            height: height,
            borderLeftStyle: walls.includes('left') ? 'solid' : 'none',
            borderTopStyle: walls.includes('up') ? 'solid' : 'none',
            borderBottomStyle: walls.includes('down') ? 'solid' : 'none',
            borderRightStyle: walls.includes('right') ? 'solid' : 'none',
            borderWidth: 2,
            borderColor: 'black',
            backgroundColor: backgroundColor,
            backgroundImage: backgroundImage
        }
    }

    return (
        <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 20, paddingBottom: 20}}>
            <div style={{ display: 'flex'}}>
                {
                    maze.map((item, x) => {
                        return (<div key={x} style={{ flexDirection: 'row' }}>
                            {item.map((item2, y) => {
                                return (<div style={getStyle(x, y, item2.walls.join(''))} key={x.toString() + y.toString()} />)
                            })}
                        </div>)
                    })
                }
            </div>
        </div>
    )
}

export default MazeDrawn