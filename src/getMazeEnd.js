const getFurthestMazeEnd = (maze) => {
    let maxDistance = { distance: 0, position: []}
    for(let [i, row] of maze.entries()) {
        for(let [i2, column] of row.entries()){
            if (column.distance > maxDistance.distance){
                maxDistance = {distance: column.distance, position: [i, i2]}
            }
        }
    }
    return {x: maxDistance.position[0], y: maxDistance.position[1]}
}

const getBottomRightEnd = (maze) => {
    const maxHeight = maze.length - 1
    const maxWidth = maze[0].length - 1
    return {x: maxHeight, y: maxWidth}
}

const getFurthestEdgeMazeEnd = (maze) => {
    const maxWidth = maze[0].length - 1
    let maxDistance = { distance: 0, position: []}
    for(let i = 0; i < maze.length; i++){
        if(i === 0 || i === (maze.length-1)){
            //whole row
            for(let [i2, column] of maze[i].entries()){
                if (column.distance > maxDistance.distance){
                    maxDistance = {distance: column.distance, position: [i, i2]}
                }
            }
        } else {
            //first and last
            let columnStart = maze[i][0]
            let columnEnd = maze[i][maxWidth]
            if (columnEnd.distance > maxDistance.distance){
                maxDistance = {distance: columnEnd.distance, position: [i, maxWidth]}
            }
            if (columnStart.distance > maxDistance.distance){
                maxDistance = {distance: columnStart.distance, position: [i, 0]}
            } 
        }
    }
    return {x: maxDistance.position[0], y: maxDistance.position[1]}
}

export {
    getFurthestMazeEnd,
    getBottomRightEnd,
    getFurthestEdgeMazeEnd
}