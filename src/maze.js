const seedrandom = require('seedrandom');
const randomWords = require('random-words');

const setUpGrid = (w, h) => {
    const grid = []
    for(let x = 0; x < w; x++){
        grid[x] = []
        for(let y = 0; y < h; y++){
            grid[x].push({visited: false, distance: 0, walls: ['up', 'down', 'left', 'right']})
        }
    }
    return grid
}

const findUnvisitedNeighbours = (x, y, w, h, grid) => {
    const neighbours = []
    const unvisited = []
    if(x > 0) {
        neighbours.push({x: x - 1, y: y})
    }
    if(y > 0) {
        neighbours.push({x: x, y: y - 1})
    }
    if(x < (w-1)) {
        neighbours.push({x: x + 1, y: y})
    }
    if(y < (h-1)) {
        neighbours.push({x: x, y: y + 1})
    }
    for(let neighbour of neighbours) {
        if(!grid[neighbour.x][neighbour.y].visited){
            unvisited.push(neighbour)
        }
    }
    return (unvisited.length > 0 ? unvisited : false);
};

const removeWall = (grid, cell, wall) => {
    grid[cell.x][cell.y].walls.splice(grid[cell.x][cell.y].walls.indexOf(wall), 1)
}

function randRange(neighbours, rng) {
    let randomNumber = rng()
    let finalSelection = Math.floor(randomNumber * ((neighbours.length)))
    return neighbours[finalSelection]
}

const mazeGenerator = (w , h, s) => {
    const width = w || 20
    const height = h || 20
    const seed = s || randomWords()
    const rng = seedrandom(seed)
    const grid = setUpGrid(width, height)
    const stack = []
    let searchDistance = 0;
    let current = {x: 0, y: 0}
    grid[current.x][current.y].visited = true

    while (current) {
        const unvisitedNeighbours = findUnvisitedNeighbours(current.x, current.y, width , height, grid);
        if (unvisitedNeighbours) {
            const neighbour = randRange(unvisitedNeighbours, rng)
                const dx = current.x - neighbour.x
                const dy = current.y - neighbour.y
            if (dx === 1) {
                removeWall(grid, current, 'left');
                removeWall(grid, neighbour, 'right');
            }
            if (dx === -1) {
                removeWall(grid, current,'right');
                removeWall(grid, neighbour, 'left');
            }
            if (dy === 1) {
                removeWall(grid, current,'up');
                removeWall(grid, neighbour, 'down');
            }
            if (dy === -1) {
                removeWall(grid, current, 'down');
                removeWall(grid, neighbour, 'up');
            }
            stack.push(neighbour);
            current = neighbour;
            searchDistance++
            grid[current.x][current.y].visited = true;
            grid[current.x][current.y].distance = searchDistance;
        } else {
            current = stack.pop();
            searchDistance--;
        }
    }
    return grid
}

exports.randRange = randRange
exports.mazeGenerator = mazeGenerator