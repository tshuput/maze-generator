import React, { useState, useEffect } from "react";
import {
  TextField,
  Button,
  Typography,
  Checkbox,
  FormControlLabel,
} from "@material-ui/core";
import RadioButtonUncheckedIcon from "@material-ui/icons/RadioButtonUnchecked";
import RadioButtonCheckedIcon from "@material-ui/icons/RadioButtonChecked";
import MazeDrawn from "./MazeDrawn";

import {
  getFurthestMazeEnd,
  getBottomRightEnd,
  getFurthestEdgeMazeEnd,
} from './getMazeEnd'

const { mazeGenerator } = require("./maze");

const HomeScreen = () => {
  const [seed, setSeed] = useState("");
  const [width, setWidth] = useState("");
  const [height, setHeight] = useState("");
  const [maze, setMaze] = useState();
  const [difficulty, setDifficulty] = useState(0);
  const [mazeEndings, setMazeEndings] = useState([]);
  const [hasGeneratedInitial, setHasGeneratedInitial] = useState(false);
  const disableGenerate = !seed || !width || !height;

  const [visitedPaths, setVisitedPaths] = useState([{}, {}, {}]);
  const [currentLocation, setCurrentLocation] = useState([
    [0, 0],
    [0, 0],
    [0, 0],
  ]);

  const handleGoLeft = () => {
    // get their current cell
    // then check if a wall is blocking them
    const x = currentLocation[difficulty][0];
    const y = currentLocation[difficulty][1];
    const cell = maze[x][y];
    const canGoLeft = !cell.walls.includes("left");
    console.log(currentLocation[difficulty]);
    console.log("current cell " + JSON.stringify(cell));
    console.log("going to [" + (x - 1) + "," + y + "]");
    if (canGoLeft) {
      // check if backtracking
      // clean up path if is backtracking
      console.log("cell going to " + JSON.stringify(maze[x - 1][y]));
      let tempVisitedPaths = [...visitedPaths];
      let isBackTracking = visitedPaths[difficulty][`x${x - 1}y${y}`];
      if (isBackTracking) {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = false;
      } else {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = true;
      }
      setVisitedPaths(tempVisitedPaths);
      let tempCurrentLocation = [...currentLocation];
      tempCurrentLocation[difficulty][0] =
        tempCurrentLocation[difficulty][0] - 1;
      setCurrentLocation(tempCurrentLocation);

      const goalX = mazeEndings[difficulty]["x"];
      const goalY = mazeEndings[difficulty]["y"];
      const didWin = x - 1 === goalX && y === goalY;
      if (didWin) {
        alert("Yay you did it!");
      }
    } else {
      console.log("can't go left");
    }
  };

  const handleGoRight = () => {
    const x = currentLocation[difficulty][0];
    const y = currentLocation[difficulty][1];
    const cell = maze[x][y];
    const canGoRight = !cell.walls.includes("right");
    if (canGoRight) {
      // check if backtracking
      // clean up path if is backtracking
      let tempVisitedPaths = [...visitedPaths];
      let isBackTracking = visitedPaths[difficulty][`x${x + 1}y${y}`];
      if (isBackTracking) {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = false;
      } else {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = true;
      }
      setVisitedPaths(tempVisitedPaths);
      let tempCurrentLocation = [...currentLocation];
      tempCurrentLocation[difficulty][0] =
        tempCurrentLocation[difficulty][0] + 1;
      setCurrentLocation(tempCurrentLocation);

      const goalX = mazeEndings[difficulty]["x"];
      const goalY = mazeEndings[difficulty]["y"];
      const didWin = x + 1 === goalX && y === goalY;
      if (didWin) {
        alert("Yay you did it!");
      }
    }
  };

  const handleGoUp = () => {
    const x = currentLocation[difficulty][0];
    const y = currentLocation[difficulty][1];
    const cell = maze[x][y];
    const canGoUp = !cell.walls.includes("up");
    if (canGoUp) {
      // check if backtracking
      // clean up path if is backtracking
      let tempVisitedPaths = [...visitedPaths];
      let isBackTracking = visitedPaths[difficulty][`x${x}y${y - 1}`];
      if (isBackTracking) {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = false;
      } else {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = true;
      }
      setVisitedPaths(tempVisitedPaths);
      let tempCurrentLocation = [...currentLocation];
      tempCurrentLocation[difficulty][1] =
        tempCurrentLocation[difficulty][1] - 1;
      setCurrentLocation(tempCurrentLocation);

      const goalX = mazeEndings[difficulty]["x"];
      const goalY = mazeEndings[difficulty]["y"];
      const didWin = y - 1 === goalY && x === goalX;
      if (didWin) {
        alert("Yay you did it!");
      }
    }
  };

  const handleGoDown = () => {
    const x = currentLocation[difficulty][0];
    const y = currentLocation[difficulty][1];
    const cell = maze[x][y];
    const canGoDown = !cell.walls.includes("down");
    if (canGoDown) {
      // check if backtracking
      // clean up path if is backtracking
      let tempVisitedPaths = [...visitedPaths];
      let isBackTracking = visitedPaths[difficulty][`x${x}y${y + 1}`];
      if (isBackTracking) {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = false;
      } else {
        tempVisitedPaths[difficulty][`x${x}y${y}`] = true;
      }
      setVisitedPaths(tempVisitedPaths);
      let tempCurrentLocation = [...currentLocation];
      tempCurrentLocation[difficulty][1] =
        tempCurrentLocation[difficulty][1] + 1;
      setCurrentLocation(tempCurrentLocation);

      const goalX = mazeEndings[difficulty]["x"];
      const goalY = mazeEndings[difficulty]["y"];
      const didWin = y + 1 === goalY && x === goalX;
      if (didWin) {
        alert("Yay you did it!");
      }
    }
  };

  const onKeyPress = (e) => {
    // Key Codes
    // Left Arrow 37
    // Up Arrow 38
    // Right Arrow 39
    // Down Arrow 40
    if (e.keyCode === 37) {
      handleGoLeft();
    }
    if (e.keyCode === 38) {
      handleGoUp();
    }
    if (e.keyCode === 39) {
      handleGoRight();
    }
    if (e.keyCode === 40) {
      handleGoDown();
    }
  };

  useEffect(() => {
    let isMounted = true;

    if (isMounted) {
      document.addEventListener("keydown", onKeyPress);
    }

    return () => {
      isMounted = false;
      document.removeEventListener("keydown", onKeyPress);
    };
  });

  const generateMaze = () => {
    setDifficulty(0);
    let tempMaze = mazeGenerator(
      width,
      height,
      seed.replace(/[^A-Z0-9]/gi, "")
    );
    let easyEnding = getBottomRightEnd(tempMaze);
    let mediumEnding = getFurthestEdgeMazeEnd(tempMaze);
    let hardEnding = getFurthestMazeEnd(tempMaze);
    let endings = [easyEnding];
    if (mediumEnding.x !== easyEnding.x || mediumEnding.y !== easyEnding.y) {
      endings.push(mediumEnding);
    }
    if (mediumEnding.x !== hardEnding.x || mediumEnding.y !== hardEnding.y) {
      endings.push(hardEnding);
    }
    setVisitedPaths([{}, {}, {}]);
    setCurrentLocation([
      [0, 0],
      [0, 0],
      [0, 0],
    ]);
    setMazeEndings(endings);
    setMaze(tempMaze);
    tempMaze.map(console.log);
  };

  const generateDefaultMaze = () => {
    setDifficulty(0);
    let tempMaze2 = mazeGenerator();
    let easyEnding = getBottomRightEnd(tempMaze2);
    let mediumEnding = getFurthestEdgeMazeEnd(tempMaze2);
    let hardEnding = getFurthestMazeEnd(tempMaze2);
    let endings = [easyEnding];
    if (mediumEnding.x !== easyEnding.x || mediumEnding.y !== easyEnding.y) {
      endings.push(mediumEnding);
    }
    if (mediumEnding.x !== hardEnding.x || mediumEnding.y !== hardEnding.y) {
      endings.push(hardEnding);
    }
    setVisitedPaths([{}, {}, {}]);
    setCurrentLocation([
      [0, 0],
      [0, 0],
      [0, 0],
    ]);
    setMazeEndings(endings);
    setMaze(tempMaze2);
  };

  if (!hasGeneratedInitial) {
    generateDefaultMaze();
    setHasGeneratedInitial(true);
  }

  const changeWidth = (e) => {
    let newWidth = e.target.value;
    if (newWidth < 1) {
      setWidth(1);
      return;
    }
    if (newWidth > 50) {
      setWidth(50);
      return;
    }
    setWidth(newWidth);
  };

  const changeHeight = (e) => {
    let newHeight = e.target.value;
    if (newHeight < 1) {
      setHeight(1);
      return;
    }
    if (newHeight > 50) {
      setHeight(50);
      return;
    }
    setHeight(newHeight);
  };

  const handleCheck = (d) => {
    console.log("difficulty changed to " + d);
    setDifficulty(d);
  };

  return (
    <div
    tabIndex="0"
    autoFocus
    onKeyDown={e => {
      console.log(e);
      if (e.key === "ArrowUp" || e.key === "ArrowDown" || e.key === "ArrowRight" || e.key === "ArrowLeft") {
        e.stopPropagation();
        e.preventDefault();
        console.log(e.key);
        return false;
      }
      e.stopPropagation();
    }}>
      <Typography variant="h2">Random Maze Generator</Typography>
      <div
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          textAlign: "center",
        }}
      >
        <Typography>Start:</Typography>
        <img
          src={null}
          style={{
            minWidth: 24,
            minHeight: 24,
            backgroundColor: "green",
            marginLeft: 10,
            marginRight: 30,
          }}
        />
        <Typography style={{ marginRight: 10 }}>Finish:</Typography>
        {/* <Typography>
                Easy: 
            </Typography>
            <img src={null} style={{ minWidth: 24, minHeight: 24, backgroundColor: 'cyan', marginLeft: 10, marginRight: 30}} />
            <Typography>
                Medium: 
            </Typography>
            <img src={null} style={{ minWidth: 24, minHeight: 24, backgroundColor: 'yellow', marginLeft: 10, marginRight: 30}} />
            <Typography>
                Hard: 
            </Typography> */}
        <img
          src={null}
          style={{
            minWidth: 24,
            minHeight: 24,
            backgroundColor: "red",
            marginLeft: 10,
            marginRight: 10,
          }}
        />
        {mazeEndings.length > 1 && (
          <>
            <Typography style={{ marginRight: 10 }}>
              Select Difficulty:
            </Typography>
            <FormControlLabel
              style={{ marginRight: 10 }}
              control={
                <Checkbox
                  icon={<RadioButtonUncheckedIcon />}
                  checkedIcon={<RadioButtonCheckedIcon />}
                  checked={difficulty === 0}
                  onChange={() => handleCheck(0)}
                  disabled={difficulty === 0}
                />
              }
              label="Easy"
            />
            <FormControlLabel
              style={{ marginRight: 10 }}
              control={
                <Checkbox
                  icon={<RadioButtonUncheckedIcon />}
                  checkedIcon={<RadioButtonCheckedIcon />}
                  checked={difficulty === 1}
                  onChange={() => handleCheck(1)}
                  disabled={difficulty === 1}
                />
              }
              label="Medium"
            />
          </>
        )}
        {mazeEndings.length > 2 && (
          <FormControlLabel
            style={{ marginRight: 10 }}
            control={
              <Checkbox
                icon={<RadioButtonUncheckedIcon />}
                checkedIcon={<RadioButtonCheckedIcon />}
                checked={difficulty === 2}
                onChange={() => handleCheck(2)}
                disabled={difficulty === 2}
              />
            }
            label="Hard"
          />
        )}
      </div>
      <Typography variant="h6">Use arrow keys to move</Typography>
      <MazeDrawn
        visitedPaths={visitedPaths}
        currentLocation={currentLocation}
        maze={maze}
        difficulty={difficulty}
        mazeEndings={mazeEndings}
      />
      <div style={{ flexDirection: "row" }}>
        <TextField
          type="text"
          label="Key"
          variant="outlined"
          value={seed}
          placeholder="any word"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(e) => setSeed(e.target.value)}
          style={{ minWidth: 250, margin: 10 }}
        />

        <TextField
          type="number"
          label="Width"
          variant="outlined"
          placeholder={"1-50"}
          min={1}
          max={50}
          value={width}
          InputLabelProps={{
            shrink: true,
          }}
          onChange={changeWidth}
          style={{ margin: 10 }}
        />

        <TextField
          type="number"
          label="Height"
          variant="outlined"
          value={height}
          placeholder={"1-50"}
          min={1}
          max={50}
          InputLabelProps={{
            shrink: true,
          }}
          style={{ margin: 10 }}
          onChange={changeHeight}
        />
      </div>
      <Button
        onClick={generateMaze}
        variant="contained"
        style={{ margin: 10 }}
        disabled={disableGenerate}
      >
        Generate Maze
      </Button>
      <Button
        onClick={generateDefaultMaze}
        variant="contained"
        style={{ margin: 10 }}
      >
        Generate Random 20x20
      </Button>
      <br />
      <div style={{ paddingLeft: "15%", paddingRight: "15%" }}>
        <Typography variant="body2">
          Welcome to my Maze Generator. Height and Width are self explanatory.
          Key is the random seed for generating the maze and can be any word or
          characters. The key starts as a random english word and width/height
          to 20 if not provided. A maze will always generate identical when
          provided the same key, width, and height. This allows you to share the
          maze by just sharing those params. Please note the key takes only
          alphanumeric characters. You can put symbols, punctuations, or spaces
          in the input but they'll be removed when passed to the generator.
          Mazes always start at the top left corner and will have up to 3
          difficulties. Easy: Bottom right corner. Medium: A random edge. Hard:
          The furthest spot from the top left corner.
        </Typography>
      </div>
    </div>
  );
};

export default HomeScreen;
