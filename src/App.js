import React from 'react';
import './App.css';
import HomeScreen from './HomeScreen';

function App() {
  return (
    <div style={{ width: '100vw', height: '100vh', textAlign: 'center'}}>
        <HomeScreen />
    </div>
  );
}

export default App;
